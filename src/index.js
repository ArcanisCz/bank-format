var parse = require('csv-parse');
var fs = require('fs');
var Promise = require('promise');
var stringify = require('csv-stringify');
var iconv = require('iconv-lite');

processFile('kb/433616550207_20150608_20170605.csv');

function processFile(path) {
    return readFile(path)
        .then(function (text) {
            if (path.indexOf("equa") > -1) {
                return parseEqua(text);
            } else if (path.indexOf("kb") > -1) {
                return parseKb(text);
            }
        })
        .then(function (result) {
            stringify(result, function (err, data) {
                saveFile(path, data);
            });
        }).catch(function (err) {
            console.error(err);
        });
}

function readFile(file) {
    if (file.indexOf("kb") > -1) {
        return new Promise(function (resolve, reject) {
            fs.readFile("./input/" + file, function (err, result) {
                if (err) {
                    reject(err);
                }
                resolve(iconv.decode(result, "win1250"));
            });
        });
    }
    return new Promise(function (resolve, reject) {
        fs.readFile("./input/" + file, encoding, function (err, result) {
            if (err) {
                reject(err);
            }
            resolve(result);
        });
    })

}

function saveFile(name, content) {
    return new Promise(function (resolve, reject) {
        fs.writeFile("./output/" + name, "Date,Payee,Category,Memo,Outflow,Inflow\n" + content, function (err) {
            if (err) {
                reject();
            }
            resolve()
        });
    })
}

function parseEqua(text) {
    return new Promise(function (resolve, reject) {
        parse(text, {
            delimiter: ";",
            from: 2
        }, function (err, output) {
            if (err) {
                reject(err)
            }
            resolve(output.map(function (row) {
                var amount = parseFloat(row[6].replace(/,/, '.'));
                return [
                    row[5].replace(/\./g, "/"), // date
                    row[15], // payee
                    "",
                    row[8] !== "Platba kartou" ? row[8] : "",
                    amount < 0 ? -amount + "" : "",
                    amount > 0 ? amount + "" : ""
                ]
            }))
        });
    });
}

function parseKb(text) {
    return new Promise(function (resolve, reject) {
        parse(text, {
            delimiter: ";",
            from: 2
        }, function (err, output) {
            if (err) {
                reject(err)
            }
            resolve(output.map(function (row) {
                var amount = parseFloat(row[4].replace(/,/, '.'));
                return [
                    row[0].replace(/\./g, "/"), // date
                    getPayee(row),
                    "",
                    "",
                    amount < 0 ? -amount + "" : "",
                    amount > 0 ? amount + "" : ""
                ];
            }));

            function getPayee(row) {
                if (
                    row[13] === "POPLATEK ZA POLOŽKY" ||
                    row[13] === "POPL.ZA VEDENÍ ÚČTU/BALÍČKU" ||
                    row[13] === "BONUS ZA VÝBĚR ATM KB" ||
                    row[13] === "BONUS ZA VEDENÍ ÚČTU/BALÍČKU" ||
                    row[13] === "POPLATEK ZA OZNÁMENÍ"
                ) {
                    return "KB";
                } else if (row[13] === "TRUST PAY, A.S.") {
                    return "Transfer: PayPal";
                } else if (row[12] === "Platba na vrub vašeho účtu") {
                    if(row[13] === "PLATBA NA VRUB VAŠEHO ÚČTU"){
                        return "";
                    }
                    return row[13].trim();
                } else if (row[12] === "Platba ve prospěch vašeho účtu") {
                    return row[13].trim();
                } else if (row[12] === "Úhrada z jiné banky") {
                    return row[13].trim();
                } else if (row[12] === "Výběr hotovosti z bankomatu") {
                    return "Transfer: Cash";
                } else if (row[12] === "Výběr z bankomatu - poplatek") {
                    return "KB";
                } else if (row[12] === "Úhrada za výpis k účtu") {
                    return "KB";
                } else {
                    return row[15].trim()
                }
            }
        });
    });
}